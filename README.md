# 2D Camera Controller

A fancy (and advanced) Godot camera controller for 2D games. The primary goal of this camera is providing tunable "juiciness" that can be toggled and tweaked to fit just about any 2D game's needs.